/*
 * Copyright (c) 2016, ARM Limited and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <arm_gic.h>
#include <assert.h>
#include <bl_common.h>
#include <console.h>
#include <debug.h>
#include <libfdt.h>
#include <gpio.h>
#include <mmio.h>
#include <platform.h>
#include <plat_params.h>
#include <plat_private.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

static struct gpio_info param_reset;
static struct gpio_info param_poweroff;
static struct bl31_apio_param param_apio;
static struct gpio_info *rst_gpio;
static struct gpio_info *poweroff_gpio;
static struct gpio_info suspend_gpio[10];
uint32_t suspend_gpio_cnt;
static struct apio_info *suspend_apio;

static uint8_t fdt_buffer[0x10000];

struct gpio_info *plat_get_rockchip_gpio_reset(void)
{
	return rst_gpio;
}

struct gpio_info *plat_get_rockchip_gpio_poweroff(void)
{
	return poweroff_gpio;
}

struct gpio_info *plat_get_rockchip_suspend_gpio(uint32_t *count)
{
	*count = suspend_gpio_cnt;

	return &suspend_gpio[0];
}

struct apio_info *plat_get_rockchip_suspend_apio(void)
{
	return suspend_apio;
}

void* plat_get_fdt(void)
{
	return &fdt_buffer[0];
}

void dt_get_reset_gpio(void)
{
	void *fdt = plat_get_fdt();
	int node = fdt_path_offset(fdt, "/config");
	const char *property = "arm-trusted-firmware,reset-gpio";
	const fdt32_t *gpio_cell;
	uint32_t gpio_num;
	int len;

	if (node < 0)
		return;

	gpio_cell = fdt_getprop(fdt, node, property, &len);
	if (!gpio_cell) {
		INFO("%s: could not find '%s' property\n",
		     __func__, property);
		return;
	}

	gpio_num = fdt32_to_cpu(*gpio_cell);
	INFO("%s: gpio# for reset is %d\n", __func__, gpio_num);

	param_reset.index = gpio_num;
	param_reset.pull_mode = BL31_GPIO_PULL_NONE;
	param_reset.polarity = BL31_GPIO_LEVEL_LOW;
	param_reset.direction = BL31_GPIO_DIR_OUT;

	rst_gpio = &param_reset;
}

void dt_relocate_pmu_firmware(void)
{
	/* TODO: ERROR HANDLING & 64bit FDT */

	void *fdt = plat_get_fdt();
	int node = fdt_path_offset(fdt, "/fit-images/pmu");
	const fdt32_t *addr_cell, *size_cell;
	int len;
	uint32_t addr, size;

	if (node < 0) {
		INFO("%s: could not find PMU firmware at /fit-images/pmu\n",
		     __func__);
		return;
	}

	addr_cell = fdt_getprop(fdt, node, "load-addr", &len);
	// if (!addr_cell)
	// handle error
	size_cell = fdt_getprop(fdt, node, "size", &len);
	addr = fdt32_to_cpu(*addr_cell);
	size = fdt32_to_cpu(*size_cell);
	INFO("%s: PMU firmware is %d bytes at %p\n",
	     __func__, size, (void*)(uintptr_t)addr);

	memcpy((void*)0xff8c0000UL /* M0_BINCODE_BASE */,
	       (void*)(uintptr_t)addr, size);
	INFO("%s: relocated PMU firmware\n", __func__);
}

static void plat_rockchip_dt_process_fdt_uart(void *fdt)
{
	const char *path_name = "/chosen";
	const char *prop_name = "stdout-path";
	int node_offset;
	int stdout_path_len;
	const char *stdout_path;
	char serial_char;
	int serial_no;
	uint32_t uart_base;

	node_offset = fdt_path_offset(fdt, path_name);
	if (node_offset < 0)
		return;

	stdout_path = fdt_getprop(fdt, node_offset, prop_name,
			&stdout_path_len);
	if (stdout_path == NULL)
		return;

	/*
	 * We expect something like:
	 *   "serial0:...""
	 */
	if (strncmp("serial", stdout_path, 6) != 0)
		return;

	serial_char = stdout_path[6];
	serial_no = serial_char - '0';

	switch (serial_no) {
	case 0:
		uart_base = UART0_BASE;
		break;
	case 1:
		uart_base = UART1_BASE;
		break;
	case 2:
		uart_base = UART2_BASE;
		break;
	case 3:
		uart_base = UART3_BASE;
		break;
	default:
		return;
	}

	rockchip_set_uart_base(uart_base);
}

bool dt_board_is_compatible(const char *board)
{
	void *fdt = plat_get_fdt();
	return fdt_node_check_compatible(fdt, 0, board) == 0;
}

static int dt_process_fdt(void* blob)
{
	int ret;
	void *fdt = plat_get_fdt();

	ret = fdt_open_into(blob, fdt, 0x10000);
	if (ret < 0) {
		INFO("%s: not a FDT\n", __func__);
		return ret;
	}

	INFO("%s: opened FDT at %p\n", __func__, blob);
	plat_rockchip_dt_process_fdt_uart(fdt);
	dt_relocate_pmu_firmware();
	dt_get_reset_gpio();

	return 0;
}

void params_early_setup(void *plat_param_from_bl2)
{
	struct bl31_plat_param *bl2_param;
	struct bl31_gpio_param *gpio_param;

	INFO("plat_param_from_bl2: %p\n", plat_param_from_bl2);

	/*
	 * Test if this is a FDT passed as a platform-specific parameter
	 * block.
	 */
	if (!dt_process_fdt(plat_param_from_bl2))
		return;

	/* keep plat parameters for later processing if need */
	bl2_param = (struct bl31_plat_param *)plat_param_from_bl2;
	while (bl2_param) {
		switch (bl2_param->type) {
		case PARAM_RESET:
			gpio_param = (struct bl31_gpio_param *)bl2_param;
			memcpy(&param_reset, &gpio_param->gpio,
			       sizeof(struct gpio_info));
			rst_gpio = &param_reset;
			break;
		case PARAM_POWEROFF:
			gpio_param = (struct bl31_gpio_param *)bl2_param;
			memcpy(&param_poweroff, &gpio_param->gpio,
				sizeof(struct gpio_info));
			poweroff_gpio = &param_poweroff;
			break;
		case PARAM_SUSPEND_GPIO:
			if (suspend_gpio_cnt >= ARRAY_SIZE(suspend_gpio)) {
				ERROR("exceed support suspend gpio number\n");
				break;
			}
			gpio_param = (struct bl31_gpio_param *)bl2_param;
			memcpy(&suspend_gpio[suspend_gpio_cnt],
			       &gpio_param->gpio,
			       sizeof(struct gpio_info));
			suspend_gpio_cnt++;
			break;
		case PARAM_SUSPEND_APIO:
			memcpy(&param_apio, bl2_param,
			       sizeof(struct bl31_apio_param));
			suspend_apio = &param_apio.apio;
			break;
		default:
			ERROR("not expected type found %ld\n",
			      bl2_param->type);
			break;
		}
		bl2_param = bl2_param->next;
	}
}
